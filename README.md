# README #

This Repository is only for FreshLIfefloral.com Website staging. We are developing the freshlifefloral.com 

### What is this repository for? ###

Only for pre development. When all functionality will work perfect we will move the staging site to our main domain
Initially the domain name is: [Staging Website url](http://development.freshlifefloral.com/)
User: admin
pass: 1234@

### How do I get set up? ###
### If you are using repository for the first time. ###
**Step-1:** 
Download the bitbicket from this url
[Bitbucket setup-Windows](https://www.atlassian.com/git/tutorials/install-git/windows)
[Bitbucket setup-Mac](https://www.atlassian.com/git/tutorials/install-git/mac-os-x)
[Bitbucket setup-Linux](https://www.atlassian.com/git/tutorials/install-git/linux)

**Step-2:** 
Clone the repository to your working directory
For example: if you are using WAMP then go to the Wamp/www/ directory through git CMD and then clone the repository.
You can see the Clone option at the top left corner.
![Screen Shot 2016-10-19 at 9.41.24 PM.png](https://bitbucket.org/repo/pbAeM6/images/1320055157-Screen%20Shot%202016-10-19%20at%209.41.24%20PM.png)
*The cloning may take some time(1/2hours for the first time)*


Step-3: 
Once you have the repository setup on your localhost now you are ready to work on your localhost.
Please note that the database will connect automatically to the Staging DB as we connect Database through Godady database server.So if you need to add new table or new attribute into database or need access please contact with **Golam Dostogir** (skype: dostogir.apu171)

### 
If repository is setup and you are working: ###

Step-1: go to the working directory. Example: if you clone the repository to c://wamp/www/ folder then you will see a folder name as "freshlifefloral-staging" so you need to goto that folder first.
Type and enter: 
```
#!bash
cd c://wamp/www/freshlifefloral-staging/
```

Step-2:
You must need to pull before you start work.Otherwise other developer updated files will be gone if you push your code without pulling.
Pleas type and enter:
```
#!bash
git pull
```

Step-3:
Now once you done your work you need to add files to the repository
Pleas type and enter:
```
#!bash
git add *
```

Step-4:
Now you need to commit the files with a nice message of what you have done
Pleas type and enter:
```
#!bash
git commit -m "Your message of what you have done"
```

Step-5:
Now you need to push the code this is the final step
Pleas type and enter:
```
#!bash
git push origin master
```

After all these step now you can check through the bitbucket.org that if your commit committed or not.
It will look like this
![Screen Shot 2016-10-19 at 10.07.12 PM.png](https://bitbucket.org/repo/pbAeM6/images/1561766715-Screen%20Shot%202016-10-19%20at%2010.07.12%20PM.png)


### Please do not forget to PULL code always before you start work ###

*If you have any question please ask Golam Dostogir