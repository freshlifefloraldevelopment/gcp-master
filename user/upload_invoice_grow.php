<?php

  // PO 2018-07-02

include("../config/config_gcp.php");

$fact_num = $_GET['id_fact'];
$growerid = $_GET['id_grow'];

$date = date('Y-m-d H:i:s');
$contador = 0;

           $update_pack = "update invoice_packing
                              set state_packing = 'P'
                            where id_fact = '" . $fact_num .     "'    ";

                mysqli_query($con,$update_pack); 

// Cabecera de Facturas

$query_grow = "select  ir.grower_id , buyer , substr(rg.growers_name,1,30) as name_grower 
                 from invoice_requests ir
                INNER JOIN growers rg ON ir.grower_id = rg.id                  
                where id_fact  = '" . $fact_num .     "' 
                  and inventary = '0'
                group by ir.grower_id , buyer , substr(rg.growers_name,1,30)  ";

    $fact_grow    = mysqli_query($con, $query_grow);
    


        while($invoice_cab = mysqli_fetch_array($fact_grow))  {

        $sel_boxg = "select id_order,offer_id_index,sum(boxqty) as totbox,count(*) as reg
                     from invoice_requests 
                    where id_fact='" . $fact_num . "'
                      and grower_id='" . $invoice_cab['grower_id'] . "' 
                      and inventary = '0'     
                    group by id_order,offer_id_index
                    order by id_order ";
        $rs_boxg = mysqli_query($con,$sel_boxg);       
          
        $totalr = mysqli_num_rows($rs_boxg);
        
        $cajas = 0;
        while($tot_boxg = mysqli_fetch_array($rs_boxg))  {

              if ($tot_boxg['totbox'] > $tot_boxg['reg']) {
                        if ($tot_boxg['reg']== 1) {
                             $cajas =  $cajas + $tot_boxg['totbox'] ;                                     
                        }else{
                             $cajas =  $cajas + ($tot_boxg['totbox']/$tot_boxg['reg']) ;                                                                 
                        }                                    
              }else{
                  $cajas = $cajas + 1 ;
              }
        }
        
        $sel_order = "select order_number  from buyer_orders  where id = '" . $fact_num . "' ";
        $rs_order = mysqli_query($con,$sel_order);       
        $border = mysqli_fetch_array($rs_order);
        
        $sel_cab = "select id_fact  from invoice_packing_cab  where id_fact='" . $fact_num . "' and grower_id='" . $invoice_cab['grower_id'] . "'  ";
        $rs_cab = mysqli_query($con,$sel_cab); 
        $verifica = mysqli_num_rows($rs_cab);
        
        ////////////////////////////////////////////////////////////////////////////////////
        if ($verifica == 0) {
            

            $contador = $contador + 1;
        
            $set_tts = "insert into invoice_packing_cab set 
                            id_fact      = '".$fact_num."', 
                            id_order     = '".$contador."', 
                            order_serial = '1', 
                            buyer        = '".$invoice_cab['buyer']."', 
                            grower_id    = '".$invoice_cab['grower_id']."', 
                            box_qty_pack = '".$cajas."', 
                            box_type     = 'HB', 
                            comment      = '".$invoice_cab['name_grower']."', 
                            date_added   = '".$date."', 
                            order_number = '".$border["order_number"]."'";

                    mysqli_query($con,$set_tts);
        }                    
          
    // Detalle de  Facturas
    
$query_grow_det = "select ir.grower_id , ir.buyer , substr(rg.growers_name,1,30) as name_grower ,ir.prod_name,ir.size,ir.steams,
                          ir.id_order , ir.offer_id_index , ir.offer_id ,
                          (boxqty) as boxes , 
                          (bunchqty) as bunchqty , gorPrice as price
                     from invoice_requests ir
                     INNER JOIN growers rg ON ir.grower_id = rg.id                  
                     where ir.id_fact  = '" . $fact_num .     "' 
                       and ir.grower_id = '" . $invoice_cab['grower_id'] . "'
                       and inventary = '0'                           
                     order by ir.offer_id";

    $fact_grow_det = mysqli_query($con, $query_grow_det);    
      $cont_det = 0;
      
      while($invoice_det = mysqli_fetch_array($fact_grow_det))  {
          
                  $sel_prod = "select id from product where name = '" . $invoice_det['prod_name'] . "' ";
                  $rs_prod = mysqli_query($con,$sel_prod);       
                  $idprod = mysqli_fetch_array($rs_prod);
                  
                  $cont_det = $cont_det + 1;
                  
            $set_det = "insert into invoice_packing set 
                            id_fact         = '".$fact_num."',  
                            id_order        = '".$contador."',  
                            order_serial    = '".$cont_det."',
                            cod_order       = '".$border["order_number"]."'       ,  
                            offer_id        = '".$invoice_det['id_order']."'      ,  
                            product         = '".$idprod["id"]."'                 ,  
                            prod_name       = '".$invoice_det['prod_name']."'     ,  
                            buyer           = '".$invoice_det['buyer']."'         ,  
                            grower_id       = '".$invoice_det['grower_id']."'     ,  
                            offer_id_index  = '".$invoice_det['offer_id_index']."',  
                            bu_qty_pack     = '".$invoice_det['bunchqty']."'      ,  
                            qty_pack        = '".$invoice_det['boxes']."'         ,  
                            comment         = '".$invoice_det['name_grower']."'   ,  
                            date_added      = '".$date."'                         ,
                            size            = '".$invoice_det['size']."'          ,  
                            steams          = '".$invoice_det['steams']   ."'     , 
                            price           = '".$invoice_det['price']    ."'     ,
                            id_grower_offer = '".$invoice_det['offer_id'] ."'  " ;

                    mysqli_query($con,$set_det);          
          
        }  
      }
       $update_tts = "update invoice_requests 
                         set inventary ='1' 
                       where id_fact  = '" . $fact_num .     "' 
                         and inventary = '0'  ";

                mysqli_query($con,$update_tts); 
                
header('location:packing_mgmt.php')
?> 