<?php
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";
        
  $fact_number = $_GET['id_fact'];
  
if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {

    header("location: index.php");
}

if (isset($_POST["Submit"]) && $_POST["Submit"] == "Save") {
    
    $ins = "update invoice_orders 
               set total_boxes   = '" . $_POST["total_boxes"]   . "', 
                   gross_weight  = '" . $_POST["gross_weight"]  . "',
                   volume_weight = '" . $_POST["volume_weight"] . "',
                   handling_lax  = '" . $_POST["handling_lax"]  . "',
                   brokerage_lax = '" . $_POST["brokerage_lax"] . "',
                   per_kg        = '" . $_POST["per_kg"] . "',
                   air_waybill   = '" . $_POST["air_waybill"]  . "',
                   charges_due_agent = '" . $_POST["charges_due_agent"] . "'                       
             where id_fact       = '" . $fact_number . "'   ";       
    
    if(mysqli_query($con, $ins))
        header("location:invoice_mgmt.php");
    
}

     $sel_invoice_cab = "select id_fact          , buyer_id        , order_number    , order_date   , shipping_method, 
                                del_date         , date_range      , is_pending      , order_serial , seen           , 
                                delivery_dates   , lfd_grower      , quick_desc      , bill_number  , gross_weight   , 
                                volume_weight    , freight_value   , per_kg          , guide_number , total_boxes    , 
                                sub_total_amount , tax_rate        , shipping_charge , handling     , air_waybill    ,
                                charges_due_agent, credit_card_fees, grand_total     , bill_state   , 
                                date_added       , user_added      , handling_lax    , brokerage_lax
                           from invoice_orders 
                          where id_fact  = '" . $fact_number . "'    ";

    $rs_invoice_cab = mysqli_query($con, $sel_invoice_cab);
    $row = mysqli_fetch_array($rs_invoice_cab);   
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <title>Admin Area</title>

        <link href="css/style.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>

        <script type="text/javascript">
            function verify()  {
                var arrTmp = new Array();
                arrTmp[0] = checkfrom();
                var i;

                _blk = true;

                for (i = 0; i < arrTmp.length; i++)  {
                    if (arrTmp[i] == false)                    {
                        _blk = false;
                    }
                }

                if (_blk == true)  {
                    return true;
                } else {
                    return false;
                }
            }

            function trim(str) {
                if (str != null) {
                    var i;
                    for (i = 0; i < str.length; i++)  {
                        if (str.charAt(i) != " ")  {
                            str = str.substring(i, str.length);
                            break;
                        }
                    }

                    for (i = str.length - 1; i >= 0; i--) {
                        if (str.charAt(i) != " ") {
                            str = str.substring(0, i + 1);
                            break;
                        }
                    }

                    if (str.charAt(0) == " ")  {
                        return "";
                    }else {
                        return str;
                    }
                }
            }

            function checkfrom()       {
                if (trim(document.frmcat.total_boxes.value) == "")  {
                    document.getElementById("lblfrom").innerHTML = "Please enter Boxes ";
                    return false;
                }else {
                    document.getElementById("lblfrom").innerHTML = "";
                    return true;
                }
                
                if (trim(document.frmcat.gross_weight.value) == "")  {
                    document.getElementById("lblgross").innerHTML = "Please enter Gross Weight ";
                    return false;
                }else {
                    document.getElementById("lblgross").innerHTML = "";
                    return true;
                }                
                
                if (trim(document.frmcat.volume_weight.value) == "")  {
                    document.getElementById("lblvolume").innerHTML = "Please enter Volume Weight ";
                    return false;
                }else {
                    document.getElementById("lblvolume").innerHTML = "";
                    return true;
                }                                
                // New
                if (trim(document.frmcat.per_kg.value) == "")  {
                    document.getElementById("lblperkg").innerHTML = "Please enter Kg. ";
                    return false;
                }else {
                    document.getElementById("lblperkg").innerHTML = "";
                    return true;
                }  
                
                if (trim(document.frmcat.air_waybill.value) == "")  {
                    document.getElementById("lblair").innerHTML = "Please enter Air Waybill ";
                    return false;
                }else {
                    document.getElementById("lblair").innerHTML = "";
                    return true;
                }   
                
                if (trim(document.frmcat.charges_due_agent.value) == "")  {
                    document.getElementById("lblcharges").innerHTML = "Please enter Charges ";
                    return false;
                }else {
                    document.getElementById("lblcharges").innerHTML = "";
                    return true;
                } 
                
                if (trim(document.frmcat.handling_lax.value) == "")  {
                    document.getElementById("lblhandling").innerHTML = "Please enter Handling Lax ";
                    return false;
                }else {
                    document.getElementById("lblhandling").innerHTML = "";
                    return true;
                }          
                
                if (trim(document.frmcat.brokerage_lax.value) == "")  {
                    document.getElementById("lblbrokerage").innerHTML = "Please enter Brokerage Lax ";
                    return false;
                }else {
                    document.getElementById("lblbrokerage").innerHTML = "";
                    return true;
                }                                                
            }

        </script>
    </head>
    <body>

        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

            <?php include("includes/header_inner.php"); ?>

            <tr>

                <td height="5"></td>

            </tr>

            <tr>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <tr>

                            <?php include("includes/agent-left.php");?>

                            <td width="5">&nbsp;</td>

                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>

                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                <tr>

                                                    <td width="10">&nbsp;</td>

                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                            <tr>

                                                                <td height="5"></td>

                                                            </tr>

                                                            <tr>

                                                                <td class="pagetitle">Edit Invoice Headboard</td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <tr>

                                                                <td>

                                                                    <table width="100%">

                                                                        <tr>

                                                                            <td>

                                                                                <a class="pagetitle1" href="invoice_mgmt.php" onclick="this.blur();"><span> Invoice</span></a>

                                                                            </td>

                                                                        </tr>

                                                                    </table>

                                                                </td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <form name="frmcat" method="post" onsubmit="return verify();" enctype="multipart/form-data" action="update_invoice_cab_edit.php?id_fact=<?php echo $row["id_fact"] ?>">

                                                                <tr>

                                                                    <td><div id="box">

                                                                            <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">

                                                                                <tr>



                                                                                    <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory </td>

                                                                                </tr>

                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Total Boxes </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="total_boxes" id="total_boxes" value="<?php echo $row["total_boxes"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblfrom"></span></td>
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Gross Weight </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="gross_weight" id="gross_weight" value="<?php echo $row["gross_weight"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblgross"></span></td>
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Volume Weight </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="volume_weight" id="volume_weight" value="<?php echo $row["volume_weight"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblvolume"></span></td>
                                                                                </tr>  
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Per Kg </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="per_kg" id="per_kg" value="<?php echo $row["per_kg"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblperkg"></span></td>
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Air Waybill </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="air_waybill" id="air_waybill" value="<?php echo $row["air_waybill"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblair"></span></td>
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Charges Due Agent </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="charges_due_agent" id="charges_due_agent" value="<?php echo $row["charges_due_agent"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblcharges"></span></td>
                                                                                </tr>                                                                                  
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Handling Lax </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="handling_lax" id="handling_lax" value="<?php echo $row["handling_lax"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblhandling"></span></td>
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Brokerage Lax </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="brokerage_lax" id="brokerage_lax" value="<?php echo $row["brokerage_lax"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblbrokerage"></span></td>
                                                                                </tr>                                                                                  
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                <tr>

                                                                                    <td>&nbsp;</td>

                                                                                    <td><input name="Submit" type="Submit" class="buttongrey" value="Save" /> </td>

                                                                                </tr>

                                                                            </table>

                                                                        </div></td>

                                                                </tr>

                                                            </form>

                                                        </table></td>

                                                    <td width="10">&nbsp;</td>

                                                </tr>

                                            </table></td>

                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

                                    </tr>

                                    <tr>

                                        <td background="images/middle-leftline.gif"></td>

                                        <td>&nbsp;</td>

                                        <td background="images/middle-rightline.gif"></td>

                                    </tr>

                                    <tr>

                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

                                        <td background="images/middle-bottomline.gif"></td>

                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

                                    </tr>

                                </table></td>

                        </tr>

                    </table></td>

            </tr>

            <tr>

                <td height="10"></td>

            </tr>

            <?php include("includes/footer-inner.php"); ?>

            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>