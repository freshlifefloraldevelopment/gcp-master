<?php
include "../config/config_gcp.php";

if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {
    header("location: index.php");
}

if (isset($_GET['delete'])) {
    $query = 'DELETE FROM shipping_method WHERE  id= ' . (int) $_GET['delete'];
    mysqli_query($con, $query);
}

if (isset($_POST["Submit"])) {
    $update = "update shipping_method set isdefault=0 where id > 0";
    mysqli_query($con, $update);

    if (isset($_POST["destination"])) {
        $udefault = "update shipping_method set isdefault=1 where id=" . $_POST["destination"];
        mysqli_query($con, $udefault);
    }
}

$qsel = "select * from shipping_method order by id";
$rs = mysqli_query($con, $qsel);
$sel_info = "select * from shipping_method where isdefault=1";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Admin Area</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <link href="css/demo_page.css" rel="stylesheet" type="text/css" />
        <link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
        <link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">

            $(document).ready(function () {

                oTable = $('#example').dataTable({
                    //"sScrollXInner": "130%",

                    "bJQueryUI": true,
                    //"sScrollY": "536",

                    "sPaginationType": "full_numbers"

                });

            });

        </script>
    </head>

    <body>
        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
            <?php include("includes/header_inner.php"); ?>
            <tr>
                <td height="5"></td>
            </tr>
            <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <?php include("includes/shipping-left.php"); ?>
                            <td width="5">&nbsp;</td>
                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td height="5"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pagetitle">Manage Shipping Methods</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td><table width="100%">
                                                                        <tr>
                                                                            <td><a href="shipping_add.php" class="pagetitle" onclick="this.blur();"><span> + Add Shipping Method </span></a></td>
                                                                        </tr>
                                                                    </table></td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td><form action="" method="post" id="form1" >  <div id="box">
                                                                            <div id="container">
                                                                                <div class="demo_jui">
                                                                                    <table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">
                                                                                        <thead>
                                                                                            <tr>
                                                                                                <th width="8%" align="center">Sr.</th>
                                                                                                <th width="20%" align="left">Shipping</th>
                                                                                                <th width="10%" align="left">Destiny</th>
                                                                                                <th width="15%" align="left">Cargo Agency</th>
                                                                                                <th width="12%" align="left">Total Connections</th>
                                                                                                <th align="center" width="10%">Edit</th>
                                                                                                <th align="center" width="14%">Delete</th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                            <?php
                                                                                            $sr = 1;
                                                                                            $temp = explode(",", $info["id"]);
                                                                                            while ($colors = mysqli_fetch_array($rs)) {
                                                                                               // print_r($colors);
                                                                                                $destiny = '';
                                                                                                if($colors['destination_country'] == 0){
                                                                                                    $destiny = 'Local';
                                                                                                }else{
                                                                                                    $getCountry = "select * from country where id='".$colors['destination_country']."'";
                                                                                                    $countryRes = mysqli_query($con,$getCountry);
                                                                                                    $country = mysqli_fetch_assoc($countryRes);
                                                                                                    $destiny = $country['name'];
                                                                                                }
                                                                                                $connections = unserialize($colors['connections']);
                                                                                                $getcon = mysqli_query($con,"select cargo_agency from connections where id='".$connections['connection_1']."'");
                                                                                                $getconRes = mysqli_fetch_assoc($getcon);
                                                                                                $cargoid = $getconRes['cargo_agency'];
                                                                                                $getCargo = mysqli_query($con,"select * from cargo_agency where id='".$cargoid."'");
                                                                                                $getcargoRes = mysqli_fetch_assoc($getCargo);
                                                                                                $getcargoname = $getcargoRes['name'];

//							  $sel_total="select id from connections where shipping_id='".$colors["id"]."'";
//							  $rs_total=mysqli_query($con,$sel_total);
//							  $total=mysqli_num_rows($rs_total);
                                                                                                ?>
                                                                                                <tr class="gradeU">
                                                                                                    <td align="center" class="text"><?= $sr; ?></td>
                                                                                                    <td class="text" align="left"><?= $colors["name"] ?></td>
                                                                                                    <td><?php echo $destiny; ?></td>
                                                                                                    <td><?php echo $getcargoname; ?></td>
                                                                                                    <td><?php echo count($connections); ?></td>
                                                                                                    <td align="center" ><a href="shipping_edit.php?id=<?= $colors["id"] ?>"><img src="images/edit.gif" border="0" alt="Edit" /></a></td>
                                                                                                    <td align="center" ><a href="?delete=<?= $colors["id"] ?>"  onclick="return confirm('Are you sure, you want to delete this shipping method ?');"><img src="images/delete.gif" border="0" alt="Delete" /></a></td>
                                                                                                </tr>
                                                                                                <?php
                                                                                                $sr++;
                                                                                            }
                                                                                            ?>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>

                                                                            <div style="float:right;margin-top:20px;">
                                                                                <input name="Submit" type="Submit" class="buttongrey" value="Update Shipping Method" />
                                                                            </div>

                                                                        </div> 
                                                                        <input type="hidden" name="abc" id="abc" value="abc" />
                                                                        <input type="hidden" name="id" id="id" value="<?= $info["id"] ?>"  />
                                                                        <input type="hidden" name="totaldestination" id="totaldestination" value="<?= $sr ?>" />
                                                                    </form></td>
                                                            </tr>
                                                        </table></td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                            </table></td>
                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
                                    </tr>
                                    <tr>
                                        <td background="images/middle-leftline.gif"></td>
                                        <td>&nbsp;</td>
                                        <td background="images/middle-rightline.gif"></td>
                                    </tr>
                                    <tr>
                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                                        <td background="images/middle-bottomline.gif"></td>
                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
                                    </tr>
                                </table></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <?php include("includes/footer-inner.php"); ?>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
