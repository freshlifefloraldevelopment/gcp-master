<?php
    // PO 31/05/2019
	include "../config/config_gcp.php";

if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {
    header("location: index.php");
}
if (isset($_POST["Submit"]) && $_POST["Submit"] == "Add") {
     $des_country     = $_POST['country'];
     $des_city        = $_POST['city'];
     $des_descripcion = $_POST['descripcion'];
     
    $insert = "insert into destiny (country,city,descripcion) values ( '".$des_country."','".$des_city."','".$des_descripcion."' )";
    if(mysqli_query($con, $insert)){
         header("location: destiny.php");
    }
}

$info = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Admin Area</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
        <script type="text/javascript">
            function verify()   {
                var arrTmp = new Array();
                
                arrTmp[0] = checkcountry();
                arrTmp[1] = checkcity();
                arrTmp[2] = checkdescripcion();                
                
                var i;

                _blk = true;

                for (i = 0; i < arrTmp.length; i++)  {

                    if (arrTmp[i] == false) {
                        _blk = false;
                    }
                }

                if (_blk == true)  {
                    return true;
                }else {
                    return false;
                }
            }

            function trim(str)  {
                if (str != null) {
                    var i;
                    for (i = 0; i < str.length; i++)  {
                        if (str.charAt(i) != " ") {
                            str = str.substring(i, str.length);
                            break;
                        }
                    }

                    for (i = str.length - 1; i >= 0; i--) {
                        if (str.charAt(i) != " ")   {
                            str = str.substring(0, i + 1);
                            break;
                        }
                    }

                    if (str.charAt(0) == " ")   {
                        return "";
                    } else {
                        return str;
                    }
                }

            }

            function checkcountry()  {
                if (trim(document.frmproduct.country.value) == "")  {
                    document.getElementById("lblcountry").innerHTML = "Please enter country ";
                    return false;
                }else{
                    document.getElementById("lblcountry").innerHTML = "";
                    return true;
                }
            }
            
            function checkcity()  {
                if (trim(document.frmproduct.city.value) == "")  {
                    document.getElementById("lblcity").innerHTML = "Please enter city ";
                    return false;
                }else{
                    document.getElementById("lblcity").innerHTML = "";
                    return true;
                }
            }            
            
            function checkdescripcion()  {
                if (trim(document.frmproduct.descripcion.value) == "")  {
                    document.getElementById("lbldescripcion").innerHTML = "Please enter descripcion destiny ";
                    return false;
                }else{
                    document.getElementById("lbldescripcion").innerHTML = "";
                    return true;
                }
            } 
</script>

<script>           
function selectscategory(){

   removeAllOptions(document.frmproduct.city);

	addOption(document.frmproduct.city,"","-- Select City --");

	  <?php

		$sel_city="select * from estado order by estadonombre";

		$res_city=mysqli_query($con,$sel_city);

		while($rw_city=mysqli_fetch_array($res_city)){

	  ?>

		if(document.frmproduct.country.value=="<?php echo $rw_city["ubicacionpaisid"]?>"){

			addOption(document.frmproduct.city,"<?php echo $rw_city["id"];?>","<?php echo $rw_city["estadonombre"];?>");

			 $('#city').val('<?php echo $_POST["city"]?>');
		}

	  <?php
		} 
	  ?>	
	}	

	function removeAllOptions(selectbox){

		var i;

		for(i=selectbox.options.length-1;i>=0;i--)	{
			selectbox.remove(i);
		}
	}

	function addOption(selectbox,value,text){

		var optn=document.createElement("OPTION");

		optn.text=text;
		optn.value=value;

		selectbox.options.add(optn);
	}            
 </script>
    </head>
    <body>
        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
<?php include("includes/header_inner.php"); ?>
            <tr>
                <td height="5"></td>
            </tr>
            <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
<?php include("includes/shipping-left.php"); ?>
                            <td width="5">&nbsp;</td>
                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td height="5"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pagetitle">Add New Destiny </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td><table width="100%">
                                                                        <tr>
                                                                            <td><a class="pagetitle1" href="destiny.php" onclick="this.blur();"><span> Manage Destiny </span></a></td>
                                                                        </tr>
                                                                    </table></td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>

                                                            <tr>
                                                                <td><div id="box">
                                                                        <form name="frmproduct" action="destiny_add.php" method="post" onsubmit="return verify();" enctype="multipart/form-data">
                                                                            <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">
                                                                                <tr>
                                                                                    <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory </td>                                    </tr>
                                                                                    
                                                                                    
                                    <tr>

                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> &nbsp; Product Category :</td>

                                      <td width="66%" bgcolor="#f2f2f2"><select name="country" id="country" onchange="selectscategory();" class="listmenu">

                                          <option value=""> -- Select Country -- </option>

                                          <?php

									     $sel_category="select * from pais order by paisnombre";

										 $rs_category=mysqli_query($con,$sel_category);

										 while($categroy=mysqli_fetch_array($rs_category))
										 { ?>

                                          <option value="<?php echo $categroy["id"]?>">

                                          <?php echo $categroy["paisnombre"]?>

                                          </option>

                                          <?php } ?>

                                        </select>
                                        <br>
                                        <span class="error" id="lblcountry"></span> </td>
                                    </tr>
                                                                                
                                    <tr>

                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> &nbsp; City :</td>

                                      <td width="66%" bgcolor="#f2f2f2"><select name="city" id="city" class="listmenu">

                                          <option value=""> -- Select City -- </option>

                                        </select>

                                        <br>

                                        <span class="error" id="lblcity"></span> </td>

                                    </tr>         

                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Destiny Description </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="descripcion" id="descripcion" value="" />
                                                                                        <br>
                                                                                            <span class="error" id="lbldescripcion"></span></td>
                                                                                </tr>                                                                                
                                                                                
                                                                              
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td><input name="Submit" type="Submit" class="buttongrey" value="Add" /></td>
                                                                                </tr>
                                                                            </table>
                                                                        </form>    
                                                                    </div></td>
                                                            </tr>

                                                        </table></td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                            </table></td>
                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
                                    </tr>
                                    <tr>
                                        <td background="images/middle-leftline.gif"></td>
                                        <td>&nbsp;</td>
                                        <td background="images/middle-rightline.gif"></td>
                                    </tr>
                                    <tr>
                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                                        <td background="images/middle-bottomline.gif"></td>
                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
                                    </tr>
                                </table></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
<?php include("includes/footer-inner.php"); ?>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
