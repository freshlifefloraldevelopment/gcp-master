/** ********************************************** **
	Your Custom Javascript File
	Put here all your custom functions
*************************************************** **/



/** Remove Panel
	Function called by app.js on panel Close (remove)
 ************************************************** **/
	function _closePanel(panel_id) {
		/** 
			EXAMPLE - LOCAL STORAGE PANEL REMOVE|UNREMOVE

			// SET PANEL HIDDEN
			localStorage.setItem(panel_id, 'closed');
			
			// SET PANEL VISIBLE
			localStorage.removeItem(panel_id);
		**/	
	}
$(document).ready(function(){
$("#show_hide").hide();
$("#show_hide1").hide();
$("#on_click").click(function(){
$("#show_hide").toggle("slow");
});
$("#on_click1").click(function(){
$("#show_hide1").toggle("slow");
});
});	
