
<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
<!DOCTYPE html>
<html lang="en" <?php language_attributes(); ?>>
<?php include("include/head.php"); ?>
<body>
<!-- main wrapper starts -->
<div class="wrapper">
  <!-- header starts -->
 <?php include("include/header.php"); ?>
  <!-- header ends -->
  <div class="cl"></div>
  <!-- About us banner starts -->
  <div class="aboutus-banner">
    <div class="banner-about">
      <div class="ban-image"><img src="<?php echo bloginfo('template_url')?>/images/abt-ban-img.jpg" width="485" height="267" alt=""> </div>
      <div class="ban-content">
        <h3>Exporting high quality Ecuadorian flowers</h3>
        <p>Ecuador is also home to some of the world’s finest and talented floral growers, creating the finest cut flowers and lush Ecuadorian plants. </p>
      </div>
    </div>
  </div>
 <div class="cl"></div>

  <!-- inner-content starts -->
  <div class="content-container inner-content">
    <div class="brad"><a href="http://www.freshlifefloral.com" class="no-active">Home</a> / <a href="http://www.freshlifefloral.com/blog/">Blog</a></div>
    <div class="cl"></div>
