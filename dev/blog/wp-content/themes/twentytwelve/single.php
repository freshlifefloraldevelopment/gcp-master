<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

<div class="left-container">
      <div class="top"></div>
      <div class="left-mid">
        <div class="content_area">

	

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>

				<nav class="nav-single">
					<h3 class="assistive-text"><?php _e( 'Post navigation', 'twentytwelve' ); ?></h3>
					<span class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'twentytwelve' ) . '</span> %title' ); ?></span>
					<span class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'twentytwelve' ) . '</span>' ); ?></span>
				</nav><!-- .nav-single -->

				<?php comments_template( '', true ); ?>

			<?php endwhile; // end of the loop. ?>

	
    
</div>
        <div class="cl"></div>
      </div>
      <div class="bot"></div>
      <div class="cl"></div>
    </div>    

<div class="right-container">

<?php get_sidebar(); ?>
<?php include("include/right.php"); ?>
<?php include("include/right2.php"); ?>
</div>
    <!-- Right Ends -->
    <div class="cl"></div>
  </div>

  <div class="footerwraper">
  <?php include("include/footer.php"); ?>
  </div>
  <!-- footer ends -->
   <?php include("include/copyright.php"); ?>
  <div class="cl"></div>
  <!-- body ends -->
</div>
<!-- main wrapper -->
<!-- Fixed-div starts -->
<?php include("include/fixeddiv.php"); ?>
