<footer>
      <nav>
        <ul class="last">
          <li><a href="index.php">Home</a></li>
          <li><a href="about-us.php">About us</a></li>
          <li><a href="our-flowers.php">Our Flower</a></li>
          <li><a href="services.php">Services</a></li>
          <li><a href="buying-strategy.php">Buying Strategy</a></li>
          <li><a href="contact-us.php">Contact us</a></li>
        </ul>
        <ul style="float:left">
          <li><a href="sitemap.php">SiteMap</a></li>
          <li><a href="privacy-page.php">Privacy Page</a></li>
          <li><a href="terms-of-use.php">Terms of Use</a></li>
          <li><a href="earning-disclaimer.php">Earnings Disclaimer</a></li>
          <li><a href="copyright-info.php">Copyright info</a></li>
        </ul>
        <div class="cl"></div>
      </nav>
      <aside class="social-network">
        <p>Newsletter</p>
        <div class="cl"></div>
        <span class="italic">Subscribe to our email newsletter for useful tips and <br>
        valuable resources</span>
    <form action="http://www.freshlifefloral.com/mcapi_listSubscribe.php" method="post">
        <div class="input_box">
          <input type="text" class="input_text" value="Your Email ID" name="text" id="text" onBlur="if (this.value == '') {this.value = 'Your Email ID';}" onFocus="if (this.value == 'Your Email ID') {this.value = '';}">
        </div>
        <div class="submit_btn">
          <input name="" type="image" src="images/submit.png">
        </div>
         <input type="hidden" name="submit" id="submit" />
   </form>

        <div class="cl"></div>
        <div class="social-icons">
          <p>Follow Us</p>
          <div class="social_icons_image"> <a href="http://www.facebook.com/"><img src="images/fb.png" width="37" height="36" alt="Facebook"></a> <a href="http://www.linkedin.com/"><img src="images/in.png" width="37" height="36" alt="Facebook"></a> <a href="https://twitter.com/"><img src="images/twitter.png" width="37" height="36" alt="Facebook"></a> </div>
        </div>
      </aside>
      <div class="cl"></div>
    </footer>
