<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

  <div class="left-container">
      <div class="top"></div>
      <div class="left-mid">
        <div class="content_area">

	
    <div id="primary" class="site-content">
		<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
				<?php comments_template( '', true ); ?>
			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->
    
   </div>
        <div class="cl"></div>
      </div>
      <div class="bot"></div>
      <div class="cl"></div>
    </div>
    
<div class="right-container">
<?php get_sidebar(); ?>
<?php include("include/right.php"); ?>
<?php include("include/right2.php"); ?>
</div>
    <!-- Right Ends -->
    <div class="cl"></div>
  </div>

  <div class="footerwraper">
  <?php include("include/footer.php"); ?>
  </div>
  <!-- footer ends -->
   <?php include("include/copyright.php"); ?>
  <div class="cl"></div>
  <!-- body ends -->
</div>
<!-- main wrapper -->
<!-- Fixed-div starts -->
<?php include("include/fixeddiv.php"); ?>
