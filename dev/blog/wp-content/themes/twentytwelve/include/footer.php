<footer>
      <nav>
        <ul class="last">
          <li><a href="http://www.freshlifefloral.com/index.php">Home</a></li>
          <li><a href="http://www.freshlifefloral.com/about-us.php">About us</a></li>
          <li><a href="http://www.freshlifefloral.com/our-flowers.php">Our Flower</a></li>
          <li><a href="http://www.freshlifefloral.com/services.php">Services</a></li>
          <li><a href="http://www.freshlifefloral.com/buying-strategy.php">Buying Strategy</a></li>
          <li><a href="http://www.freshlifefloral.com/contact-us.php">Contact us</a></li>
        </ul>
        <ul style="float:left">
          <li><a href="http://www.freshlifefloral.com/sitemap.php">SiteMap</a></li>
          <li><a href="http://www.freshlifefloral.com/privacy-page.php">Privacy Page</a></li>
          <li><a href="http://www.freshlifefloral.com/terms-of-use.php">Terms of Use</a></li>
          <li><a href="http://www.freshlifefloral.com/earning-disclaimer.php">Earnings Disclaimer</a></li>
          <li><a href="http://www.freshlifefloral.com/copyright-info.php">Copyright info</a></li>
        </ul>
        <div class="cl"></div>
      </nav>
      <aside class="social-network">
        <p>Newsletter</p>
        <div class="cl"></div>
        <span class="italic">Subscribe to our email newsletter for useful tips and <br>
        valuable resources</span>
         <form action="http://www.freshlifefloral.com/mcapi_listSubscribe.php" method="post">
        <div class="input_box">
          <input type="text" class="input_text" value="Your Email ID" name="text" id="text" onBlur="if (this.value == '') {this.value = 'Your Email ID';}" onFocus="if (this.value == 'Your Email ID') {this.value = '';}">
        </div>
        <div class="submit_btn">
          <input name="" type="image" src="<?php echo bloginfo('template_url'); ?>/images/submit.png">
        </div>
         <input type="hidden" name="submit" id="submit" />
   </form>
        <div class="cl"></div>
        <div class="social-icons">
          <p>Follow Us</p>
          <div class="social_icons_image"> <a href="http://www.facebook.com/"><img src="<?php echo bloginfo('template_url')?>/images/fb.png" width="37" height="36" alt="Facebook"></a> <a href="http://www.linkedin.com/"><img src="<?php echo bloginfo('template_url')?>/images/in.png" width="37" height="36" alt="Facebook"></a> <a href="https://twitter.com/"><img src="<?php echo bloginfo('template_url')?>/images/twitter.png" width="37" height="36" alt="Facebook"></a> </div>
        </div>
      </aside>
      <div class="cl"></div>
    </footer>
