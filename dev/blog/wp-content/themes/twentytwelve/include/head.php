<?php
     
    $sel_default_meta="select * from page_mgmt where page_id=10";
	$rs_default_meta=mysql_query($sel_default_meta);
	$default_meta=mysql_fetch_array($rs_default_meta);
?>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><? if($page["meta_title"]=="") { echo $default_meta["meta_title"]; } else { echo $page["meta_title"]; }?></title>
<meta name="keywords" content="<? if($page["meta_keyword"]=="") { echo $default_meta["meta_keyword"]; } else { echo $page["meta_keyword"]; }?>" />
<meta name="description" content="<? if($page["meta_desc"]=="") { echo $default_meta["meta_desc"]; } else { echo $page["meta_desc"]; }?>" />
<link href="<?php echo bloginfo('template_url')?>/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo bloginfo('template_url')?>/css/style.css" rel="stylesheet" type="text/css" />

<link href="<?php echo bloginfo('template_url')?>/css/slider1.css" rel="stylesheet" type="text/css" />
<link href="<?php echo bloginfo('template_url')?>/css/new.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo bloginfo('template_url')?>/js/html5.js"></script>
<script type="text/javascript" src="<?php echo bloginfo('template_url')?>/js/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('template_url')?>/css/jquery.lightbox-0.5.css" media="screen" />
<!-- <script type="text/javascript" src="<?php echo bloginfo('template_url')?>/js/jquery.lightbox-0.5.js"></script>
<script type="text/javascript">
    $(function() {
        $('#gallery a').lightBox({
		containerResizeSpeed: 350,
	txtImage: 'Image',
	txtOf: 'of'
		});
		
    });
    </script> -->
<script type="text/javascript" src="<?php echo bloginfo('template_url')?>/js/jquery.cycle.all.js"></script>
<script type="text/javascript">
$(document).ready(function(e) {
 	$("#mycarousel").jcarousel({
       scroll: 1,
        initCallback: mycarousel_initCallback,
        // This tells jCarousel NOT to autobuild prev/next buttons
        buttonNextHTML: null,
        buttonPrevHTML: null
	});
	$('#slides_banner ul').cycle({
		next:true,
		prev:true,
		next:'#next', 
    	prev:'#prev' 
	});
    
});
function mycarousel_initCallback(carousel) 
{
    jQuery('.jcarousel-control a').bind('click', function()
	 {
	  carousel.scroll(jQuery.jcarousel.intval(jQuery(this).text()));
		return false;
    });
};
</script>
<!-- Java Slide -->
<script type="text/javascript" src="<?php echo bloginfo('template_url')?>/js/jquery.jcarousel.min.js"></script>
<script>
$(document).ready(function() {

	//rotation speed and timer
	var speed = 2000;
	var run = setInterval('rotate()', speed);	
	var liCount = $('#slides1 li').length;
	
	if(liCount<4) 
	{
		$('#prevS3,#nextS3').hide();
	}
	else
	{
	
	
	//grab the width and calculate left value
	var item_width = $('#slides1 li').outerWidth(); 
	var left_value = item_width * (-1); 
        
    //move the last item before first item, just in case user click prev button
	$('#slides1 li:first').before($('#slides1 li:last'));
	
	//set the default item to the correct position 
	$('#slides1 ul').css({'left' : left_value});

    //if user clicked on prev button
	$('#prevS3').click(function() {

		//get the right position            
		var left_indent = parseInt($('#slides1 ul').css('left')) + item_width;

		//slide the item            
		$('#slides1 ul:not(:animated)').animate({'left' : left_indent}, 200,function(){    

            //move the last item and put it as first item            	
			$('#slides1 li:first').before($('#slides1 li:last'));           

			//set the default item to correct position
			$('#slides1 ul').css({'left' : left_value});
		
		});

		//cancel the link behavior            
		return false;
            
	});

 
    //if user clicked on next button
	$('#nextS3').click(function() {
		
		//get the right position
		var left_indent = parseInt($('#slides1 ul').css('left')) - item_width;
		
		//slide the item
		$('#slides1 ul:not(:animated)').animate({'left' : left_indent}, 400, function () {
            
            //move the first item and put it as last item
			$('#slides1 li:last').after($('#slides1 li:first'));                 	
			
			//set the default item to correct position
			$('#slides1 ul').css({'left' : left_value});
		
		});
		         
		//cancel the link behavior
		return false;
		
	});        
		clearInterval(run);
	//if mouse hover, pause the auto rotation, otherwise rotate it
	$('#slides1').hover(
		
		function() {
			clearInterval(run);
		}, 
		function() {
		//	run = setInterval('rotate()', speed);	
		}
	); 
	};
});

//a simple function to click next link
//a timer will call this function, and the rotation will begin :)  
function rotate() {
	$('#nextS3').click();
}
  
        
</script>
<!-- color box starts -->
<link type="text/css" media="screen" rel="stylesheet" href="<?php echo bloginfo('template_url')?>/css/colorbox.css" />
<script type="text/javascript" src="<?php echo bloginfo('template_url')?>/js/jquery.colorbox.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
            $(".example6").colorbox({iframe:true, innerWidth:335, innerHeight:450});
            $(".example5").colorbox({iframe:true, innerWidth:335, innerHeight:450});
            $(".example7").colorbox({iframe:true, innerWidth:335, innerHeight:230});
            $(".example9").colorbox({iframe:true, innerWidth:335, innerHeight:315});
            //jQuery(".example8").colorbox({width:"30%", inline:true, href:"#inline_example1"});
	});
</script>

<script language="javascript">
function validate_qcontact() { 
	if(document.qform.qfull_name.value=="" || document.qform.qfull_name.value=="Full Name") {
		alert("Please enter your full name.\n");
		document.qform.qfull_name.focus();
		return false;
	}
	if(document.qform.qemail_id.value==""){
		alert("Please enter your email address.\n");
		document.qform.qemail_id.focus();
		return false;
	}
	if(!IsEmail(document.qform.qemail_id.value ))
	{
	    alert("Please enter your valid email address.\n");
		document.qform.qemail_id.focus();
		return false;
	}
	if(document.qform.qphone_no.value==""){
		alert("Please enter your phone no.\n");
		document.qform.qphone_no.focus();
		return false;
	}
	if(document.qform.qdescription.value=="") {
		alert("Please enter your brief.\n");
		document.qform.qdescription.focus();
		return false;
	}
 else{
	  return true;
	 }
}
function IsEmail(mail)
{
  var text  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return text.test(mail)
}
</script>
<!-- water mark starts -->
<script type="text/javascript" src="<?php echo bloginfo('template_url')?>/js/Watermark.js"></script>
<script type="text/javascript">
   var Init = function() {
                watermark("qfull_name", "Full Name");           
                watermark("qemail_id", "Email Address");           
                watermark("qphone_no", "Phone No.");           
                watermark("qdescription", "Your Brief");           
            }
            if (window.addEventListener)
                window.addEventListener("load", Init, false)
            else if (window.attachEvent)
                window.attachEvent("onload", Init)
            else if (document.getElementById)
                window.onload = Init;
				
		   $(document).ready(function() {

	//Default Action
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content
	
	//On Click Event
	$("ul.tabs li").click(function() {
		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content
		var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active content
		return false;
	});

});
		
</script>
<!-- water mark ends -->

<script type="text/javascript">
/* jQuery Mega Menu v1.02
* Last updated: June 29th, 2009. This notice must stay intact for usage 
* Author: JavaScript Kit at http://www.javascriptkit.com/
* Visit http://www.javascriptkit.com/script/script2/jScale/ for full source code
*/

var jkmegamenu={

effectduration: 300, //duration of animation, in milliseconds
delaytimer: 200, //delay after mouseout before menu should be hidden, in milliseconds

//No need to edit beyond here
megamenulabels: [],
megamenus: [], //array to contain each block menu instances
zIndexVal: 1000, //starting z-index value for drop down menu
$shimobj: null,

addshim:function($){
	$(document.body).append('<IFRAME id="outlineiframeshim" src="'+(location.protocol=="https:"? 'blank.htm' : 'about:blank')+'" style="display:none; left:0; top:0; z-index:999; position:absolute; filter:progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)" frameBorder="0" scrolling="no"></IFRAME>')
	this.$shimobj=$("#outlineiframeshim")
},

alignmenu:function($, e, megamenu_pos){
	var megamenu=this.megamenus[megamenu_pos]
	var $anchor=megamenu.$anchorobj
	var $menu=megamenu.$menuobj
	var menuleft=($(window).width()-(megamenu.offsetx-$(document).scrollLeft())>megamenu.actualwidth)? megamenu.offsetx : megamenu.offsetx-megamenu.actualwidth+megamenu.anchorwidth //get x coord of menu
	//var menutop=($(window).height()-(megamenu.offsety-$(document).scrollTop()+megamenu.anchorheight)>megamenu.actualheight)? megamenu.offsety+megamenu.anchorheight : megamenu.offsety-megamenu.actualheight
	var menutop=megamenu.offsety+megamenu.anchorheight  //get y coord of menu
	menutop=202;
	menuleft=280;
	$menu.css({left:menuleft+"px", top:menutop+"px"})
	this.$shimobj.css({width:megamenu.actualwidth+"px", height:megamenu.actualheight+"px", left:480+"px", top:menutop+"px", display:"block"})
},

showmenu:function(e, megamenu_pos){
	var megamenu=this.megamenus[megamenu_pos]
	var $menu=megamenu.$menuobj
	var $menuinner=megamenu.$menuinner
	if ($menu.css("display")=="none"){
		this.alignmenu(jQuery, e, megamenu_pos)
		$menu.css("z-index", ++this.zIndexVal)
		$menu.show(this.effectduration, function(){
			$menuinner.css('visibility', 'visible')
		})
	}
	else if ($menu.css("display")=="block" && e.type=="click"){ //if menu is hidden and this is a "click" event (versus "mouseout")
		this.hidemenu(e, megamenu_pos)
	}
	return false
},

hidemenu:function(e, megamenu_pos){
	var megamenu=this.megamenus[megamenu_pos]
	var $menu=megamenu.$menuobj
	var $menuinner=megamenu.$menuinner
	$menuinner.css('visibility', 'hidden')
	this.$shimobj.css({display:"none", left:0, top:0})
	$menu.hide(this.effectduration)
},

definemenu:function(anchorid, menuid, revealtype){
	this.megamenulabels.push([anchorid, menuid, revealtype])
},

render:function($){
	for (var i=0, labels=this.megamenulabels[i]; i<this.megamenulabels.length; i++, labels=this.megamenulabels[i]){
		if ($('#'+labels[0]).length!=1 || $('#'+labels[1]).length!=1) //if one of the two elements are NOT defined, exist
			return
		this.megamenus.push({$anchorobj:$("#"+labels[0]), $menuobj:$("#"+labels[1]), $menuinner:$("#"+labels[1]).children('ul:first-child'), revealtype:labels[2], hidetimer:null})
		var megamenu=this.megamenus[i]	
		megamenu.$anchorobj.add(megamenu.$menuobj).attr("_megamenupos", i+"pos") //remember index of this drop down menu
		megamenu.actualwidth=megamenu.$menuobj.outerWidth()
		megamenu.actualheight=megamenu.$menuobj.outerHeight()
		megamenu.offsetx=megamenu.$anchorobj.offset().left
		megamenu.offsety=megamenu.$anchorobj.offset().top
		megamenu.anchorwidth=megamenu.$anchorobj.outerWidth()
		megamenu.anchorheight=megamenu.$anchorobj.outerHeight()
		$(document.body).append(megamenu.$menuobj) //move drop down menu to end of document
		megamenu.$menuobj.css("z-index", ++this.zIndexVal).hide()
		megamenu.$menuinner.css("visibility", "hidden")
		megamenu.$anchorobj.bind(megamenu.revealtype=="click"? "click" : "mouseenter", function(e){
			var menuinfo=jkmegamenu.megamenus[parseInt(this.getAttribute("_megamenupos"))]
			clearTimeout(menuinfo.hidetimer) //cancel hide menu timer
			return jkmegamenu.showmenu(e, parseInt(this.getAttribute("_megamenupos")))
		})
		megamenu.$anchorobj.bind("mouseleave", function(e){
			var menuinfo=jkmegamenu.megamenus[parseInt(this.getAttribute("_megamenupos"))]
			if (e.relatedTarget!=menuinfo.$menuobj.get(0) && $(e.relatedTarget).parents("#"+menuinfo.$menuobj.get(0).id).length==0){ //check that mouse hasn't moved into menu object
				menuinfo.hidetimer=setTimeout(function(){ //add delay before hiding menu
					jkmegamenu.hidemenu(e, parseInt(menuinfo.$menuobj.get(0).getAttribute("_megamenupos")))
				}, jkmegamenu.delaytimer)
			}
		})
		megamenu.$menuobj.bind("mouseenter", function(e){
			var menuinfo=jkmegamenu.megamenus[parseInt(this.getAttribute("_megamenupos"))]
			clearTimeout(menuinfo.hidetimer) //cancel hide menu timer
		})
		megamenu.$menuobj.bind("click mouseleave", function(e){
			var menuinfo=jkmegamenu.megamenus[parseInt(this.getAttribute("_megamenupos"))]
			menuinfo.hidetimer=setTimeout(function(){ //add delay before hiding menu
				jkmegamenu.hidemenu(e, parseInt(menuinfo.$menuobj.get(0).getAttribute("_megamenupos")))
			}, jkmegamenu.delaytimer)
		})
	} //end for loop
	if(/Safari/i.test(navigator.userAgent)){ //if Safari
		$(window).bind("resize load", function(){
			for (var i=0; i<jkmegamenu.megamenus.length; i++){
				var megamenu=jkmegamenu.megamenus[i]
				var $anchorisimg=(megamenu.$anchorobj.children().length==1 && megamenu.$anchorobj.children().eq(0).is('img'))? megamenu.$anchorobj.children().eq(0) : null
				if ($anchorisimg){ //if anchor is an image link, get offsets and dimensions of image itself, instead of parent A
					megamenu.offsetx=$anchorisimg.offset().left
					megamenu.offsety=$anchorisimg.offset().top
					megamenu.anchorwidth=$anchorisimg.width()
					megamenu.anchorheight=$anchorisimg.height()
				}
			}
		})
	}
	else{
		$(window).bind("resize", function(){
			for (var i=0; i<jkmegamenu.megamenus.length; i++){
				var megamenu=jkmegamenu.megamenus[i]	
				megamenu.offsetx=megamenu.$anchorobj.offset().left
				megamenu.offsety=megamenu.$anchorobj.offset().top
			}
		})
	}
	jkmegamenu.addshim($)
}

}

jQuery(document).ready(function($){
	jkmegamenu.render($)
})
</script>

<script type="text/javascript">
/* jQuery Mega Menu v1.02
* Last updated: June 29th, 2009. This notice must stay intact for usage 
* Author: JavaScript Kit at http://www.javascriptkit.com/
* Visit http://www.javascriptkit.com/script/script2/jScale/ for full source code
*/

var jkmegamenu2={

effectduration: 300, //duration of animation, in milliseconds
delaytimer: 200, //delay after mouseout before menu should be hidden, in milliseconds

//No need to edit beyond here
megamenulabels: [],
megamenus: [], //array to contain each block menu instances
zIndexVal: 1000, //starting z-index value for drop down menu
$shimobj: null,

addshim:function($){
	$(document.body).append('<IFRAME id="outlineiframeshim" src="'+(location.protocol=="https:"? 'blank.htm' : 'about:blank')+'" style="display:none; left:0; top:0; z-index:999; position:absolute; filter:progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)" frameBorder="0" scrolling="no"></IFRAME>')
	this.$shimobj=$("#outlineiframeshim")
},

alignmenu:function($, e, megamenu_pos){
	var megamenu=this.megamenus[megamenu_pos]
	var $anchor=megamenu.$anchorobj
	var $menu=megamenu.$menuobj
	var menuleft=($(window).width()-(megamenu.offsetx-$(document).scrollLeft())>megamenu.actualwidth)? megamenu.offsetx : megamenu.offsetx-megamenu.actualwidth+megamenu.anchorwidth //get x coord of menu
	//var menutop=($(window).height()-(megamenu.offsety-$(document).scrollTop()+megamenu.anchorheight)>megamenu.actualheight)? megamenu.offsety+megamenu.anchorheight : megamenu.offsety-megamenu.actualheight
	var menutop=megamenu.offsety+megamenu.anchorheight  //get y coord of menu
	menutop=202;
	menuleft=840;
	$menu.css({left:menuleft+"px", top:menutop+"px"})
	this.$shimobj.css({width:megamenu.actualwidth+"px", height:megamenu.actualheight+"px", left:840+"px", top:menutop+"px", display:"block"})
},

showmenu:function(e, megamenu_pos){
	var megamenu=this.megamenus[megamenu_pos]
	var $menu=megamenu.$menuobj
	var $menuinner=megamenu.$menuinner
	if ($menu.css("display")=="none"){
		this.alignmenu(jQuery, e, megamenu_pos)
		$menu.css("z-index", ++this.zIndexVal)
		$menu.show(this.effectduration, function(){
			$menuinner.css('visibility', 'visible')
		})
	}
	else if ($menu.css("display")=="block" && e.type=="click"){ //if menu is hidden and this is a "click" event (versus "mouseout")
		this.hidemenu(e, megamenu_pos)
	}
	return false
},

hidemenu:function(e, megamenu_pos){
	var megamenu=this.megamenus[megamenu_pos]
	var $menu=megamenu.$menuobj
	var $menuinner=megamenu.$menuinner
	$menuinner.css('visibility', 'hidden')
	this.$shimobj.css({display:"none", left:0, top:0})
	$menu.hide(this.effectduration)
},

definemenu:function(anchorid, menuid, revealtype){
	this.megamenulabels.push([anchorid, menuid, revealtype])
},

render:function($){
	for (var i=0, labels=this.megamenulabels[i]; i<this.megamenulabels.length; i++, labels=this.megamenulabels[i]){
		if ($('#'+labels[0]).length!=1 || $('#'+labels[1]).length!=1) //if one of the two elements are NOT defined, exist
			return
		this.megamenus.push({$anchorobj:$("#"+labels[0]), $menuobj:$("#"+labels[1]), $menuinner:$("#"+labels[1]).children('ul:first-child'), revealtype:labels[2], hidetimer:null})
		var megamenu=this.megamenus[i]	
		megamenu.$anchorobj.add(megamenu.$menuobj).attr("_megamenupos", i+"pos") //remember index of this drop down menu
		megamenu.actualwidth=megamenu.$menuobj.outerWidth()
		megamenu.actualheight=megamenu.$menuobj.outerHeight()
		megamenu.offsetx=megamenu.$anchorobj.offset().left
		megamenu.offsety=megamenu.$anchorobj.offset().top
		megamenu.anchorwidth=megamenu.$anchorobj.outerWidth()
		megamenu.anchorheight=megamenu.$anchorobj.outerHeight()
		$(document.body).append(megamenu.$menuobj) //move drop down menu to end of document
		megamenu.$menuobj.css("z-index", ++this.zIndexVal).hide()
		megamenu.$menuinner.css("visibility", "hidden")
		megamenu.$anchorobj.bind(megamenu.revealtype=="click"? "click" : "mouseenter", function(e){
			var menuinfo=jkmegamenu2.megamenus[parseInt(this.getAttribute("_megamenupos"))]
			clearTimeout(menuinfo.hidetimer) //cancel hide menu timer
			return jkmegamenu2.showmenu(e, parseInt(this.getAttribute("_megamenupos")))
		})
		megamenu.$anchorobj.bind("mouseleave", function(e){
			var menuinfo=jkmegamenu2.megamenus[parseInt(this.getAttribute("_megamenupos"))]
			if (e.relatedTarget!=menuinfo.$menuobj.get(0) && $(e.relatedTarget).parents("#"+menuinfo.$menuobj.get(0).id).length==0){ //check that mouse hasn't moved into menu object
				menuinfo.hidetimer=setTimeout(function(){ //add delay before hiding menu
					jkmegamenu2.hidemenu(e, parseInt(menuinfo.$menuobj.get(0).getAttribute("_megamenupos")))
				}, jkmegamenu2.delaytimer)
			}
		})
		megamenu.$menuobj.bind("mouseenter", function(e){
			var menuinfo=jkmegamenu2.megamenus[parseInt(this.getAttribute("_megamenupos"))]
			clearTimeout(menuinfo.hidetimer) //cancel hide menu timer
		})
		megamenu.$menuobj.bind("click mouseleave", function(e){
			var menuinfo=jkmegamenu2.megamenus[parseInt(this.getAttribute("_megamenupos"))]
			menuinfo.hidetimer=setTimeout(function(){ //add delay before hiding menu
				jkmegamenu2.hidemenu(e, parseInt(menuinfo.$menuobj.get(0).getAttribute("_megamenupos")))
			}, jkmegamenu2.delaytimer)
		})
	} //end for loop
	if(/Safari/i.test(navigator.userAgent)){ //if Safari
		$(window).bind("resize load", function(){
			for (var i=0; i<jkmegamenu2.megamenus.length; i++){
				var megamenu=jkmegamenu2.megamenus[i]
				var $anchorisimg=(megamenu.$anchorobj.children().length==1 && megamenu.$anchorobj.children().eq(0).is('img'))? megamenu.$anchorobj.children().eq(0) : null
				if ($anchorisimg){ //if anchor is an image link, get offsets and dimensions of image itself, instead of parent A
					megamenu.offsetx=$anchorisimg.offset().left
					megamenu.offsety=$anchorisimg.offset().top
					megamenu.anchorwidth=$anchorisimg.width()
					megamenu.anchorheight=$anchorisimg.height()
				}
			}
		})
	}
	else{
		$(window).bind("resize", function(){
			for (var i=0; i<jkmegamenu2.megamenus.length; i++){
				var megamenu=jkmegamenu2.megamenus[i]	
				megamenu.offsetx=megamenu.$anchorobj.offset().left
				megamenu.offsety=megamenu.$anchorobj.offset().top
			}
		})
	}
	jkmegamenu2.addshim($)
}

}

jQuery(document).ready(function($){
	jkmegamenu2.render($)
})
</script>

<script type="text/javascript">

//jkmegamenu.definemenu("anchorid", "menuid", "mouseover|click")
jkmegamenu.definemenu("megaanchor", "megamenu1", "mouseover")
jkmegamenu2.definemenu("megaanchor2", "megamenu2", "mouseover")
</script>
<!-- color box ends -->
</head>
